##
## view.py
## Login : <freyes@yoda>
## Started on  Tue Jul  1 20:51:33 2008 Felipe Reyes
## $Id$
##
## Copyright (C) 2008 Felipe Reyes
##
## This file is part of Marvin.
##
## Marvin is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Marvin is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

from marvin.model import Photo, PhotoListStore, connect_to_db, ConfigurationManager

import locale
import gettext
import gobject
import gtk
import gtk.glade
import gtk.gdk
import gnomevfs
import gconf
import threading
import sqlite3
import os
import sys
import shutil
import time
import datetime
try:
    import EXIF
except:
    curr_dir = os.path.abspath(".")
    sys.path.append(curr_dir)
    import EXIF

##logging system
import logging
log = None

### l18n
APP_NAME = "marvin"
#Get the local directory since we are not installing anything
local_path = os.path.realpath(os.path.dirname(sys.argv[0]))
# Init the list of languages to support
langs = []
#Check the default locale
lc, encoding = locale.getdefaultlocale()
if (lc):
    #If we have a default, it's the first in the list
    langs = [lc]
# Now lets get all of the supported languages on the system
language = os.environ.get('LANGUAGE', None)
if (language):
    langs += language.split(":")

langs += ["es_CL"]

gettext.bindtextdomain(APP_NAME, local_path)
gettext.textdomain(APP_NAME)
# Get the language to use
lang = gettext.translation(APP_NAME, local_path, languages=langs, fallback = True)

_ = lang.gettext
###

gtk.gdk.threads_init()
wTree = None

from pkg_resources import resource_filename


class MainWindow (object):

    def __init__ (self, **kwargs):

        self._gladefile = resource_filename("marvin.ui", "marvin.glade")
        global wTree
        wTree = gtk.glade.XML(self._gladefile, "wnd_main")

        signals = {
            "on_entry_sql_filter_activate" : self._on_sql_filter_activate,
            "on_btn_search_with_filter_clicked" : self._on_search_clicked,
            "on_iconview1_selection_changed" : self._on_selection_changed,
            "on_wnd_main_delete_event" : self._on_delete_event,
            "on_iconview1_item_activated" : self._item_activated,
            "on_iconview1_button_press_event" : self._show_popup,
            }

        wTree.signal_autoconnect(signals)

        list_photos = gtk.ListStore(Photo, gtk.gdk.Pixbuf, str)
        list_photos.clear()

        #setup the iconview and set the model
        iconview = wTree.get_widget("iconview1")
        iconview.set_model(list_photos)
        iconview.set_pixbuf_column(1)
        iconview.set_text_column(2)

        self._win = wTree.get_widget("wnd_main")
        uidef = resource_filename("marvin.ui", "bar.xml")
        self._uimanager = gtk.UIManager()
        accel_group = self._uimanager.get_accel_group()
        self._win.add_accel_group(accel_group)
        action_group = gtk.ActionGroup('my_actions')
        action_group.add_actions([#TODO:finish actions
            ('File', None, _("_File"), None, None, None),
            ('ImportPhoto', gtk.STOCK_ADD, _("Import Photos"), "<Control>i", None,
             self._on_import_clicked),
            ("Quit", gtk.STOCK_QUIT, None, "<Control>q", None,
             self._on_quit_clicked),
            ("Image", None, _("_Image"), None, None, None),
            ('SetAsWallpaper', None, _("Set as wallpaper"), None, None,
             self._on_set_wallpaper_clicked),
            ("Help", None, _("Help"), None, None, None),
            ("About", gtk.STOCK_ABOUT, None, None, None,
             self._on_about_clicked),
            ])

        self._uimanager.insert_action_group(action_group, 0)
        self._uimanager.add_ui_from_file(uidef)
        box = wTree.get_widget("vbox1")
        menubar = self._uimanager.get_widget("/menubar")
        box.pack_start(menubar, False)
        box.reorder_child(menubar, 0)
        toolbar = self._uimanager.get_widget("/toolbar")
        box.pack_start(toolbar, False)
        box.reorder_child(toolbar, 1)

        config = ConfigurationManager()
        self._win.set_default_size(config.window_default_size[0],
                                   config.window_default_size[1])
        self._win.show_all()


        # prepare and launch the thread that fills the iconview model
        self._thread = PhotoListThread(list_photos)
        self._thread.start()

    def _show_popup(self, widget, event):
        if event.button != 3:
            return

        iconview = wTree.get_widget("iconview1")
        selected_items = iconview.get_selected_items()
        item_under_mouse = iconview.get_item_at_pos (int(event.x), int(event.y))[0]

        if item_under_mouse not in selected_items:
            iconview.unselect_all()
            iconview.select_path(item_under_mouse)
            selected_items = iconview.get_selected_items()

        item = self._uimanager.get_widget("/popup/SetAsWallpaper")
        if len(selected_items) != 1:
            item.set_sensitive(False)
        else:
            item.set_sensitive(True)

        menu_widget = self._uimanager.get_widget("/popup");

        menu_widget.popup(None, None, None, event.button, event.time);
        menu_widget.show_all()

    def _on_about_clicked(self, menuitem):
        AboutDialog()

    def _on_quit_clicked (self, menuitem):
        self.quit()

    def _on_set_wallpaper_clicked(self, menuitem):

        iconview = wTree.get_widget("iconview1")

        selected_items = iconview.get_selected_items()

        if len(selected_items) != 1:
            return

        model = iconview.get_model()
        iter = model.get_iter(selected_items[0])
        photo = model.get(iter, 0)[0]

        ConfigurationManager.set_wallpaper(photo.path)

    def _on_import_clicked(self, menuitem):
        ImportWindow()

    def _on_sql_filter_activate(self, entry):
        button = wTree.get_widget("btn_search_with_filter")
        button.clicked()

    def _on_search_clicked(self, button):
        """

        Arguments:
        - `self`:
        - `button`:
        """
        model = wTree.get_widget("iconview1").get_model()

        entry = wTree.get_widget("entry_sql_filter")
        sql_filter = entry.get_text()

        query = "select id, uri from photos " + sql_filter

        if not self._thread.stop.isSet():
            self._thread.stop.set()
        thread = PhotoListThread(model, query)
        thread.start()

    def _on_selection_changed(self, iconview):
        """

        Arguments:
        - `self`:
        - `iconview`:
        """
        num_selec_items = len(iconview.get_selected_items())
        if num_selec_items == 1:

            lbl = wTree.get_widget("lbl_original_date")

            model = iconview.get_model()
            iter = model.get_iter(iconview.get_selected_items()[0])
            photo = model.get(iter, 0)[0]
            str_date = photo.original_date.strftime("%x")
            lbl.set_text(_("Original Date %s" % str_date))

            lbl = wTree.get_widget("lbl_tags")

            lbl.set_text(str(photo.tags))
        elif num_selec_items == 0:
            wTree.get_widget("lbl_original_date").set_text("")
            wTree.get_widget("lbl_tags").set_text("")

    def _item_activated(self, iconview, path):

        store = iconview.get_model()
        iter = store.get_iter(path)
        photo = store.get_value (iter, 0)

        ImageWindow(photo, iconview)

    def _on_delete_event (self, widget, event):
        self.quit()

    def quit(self):
        config = ConfigurationManager()
        win = wTree.get_widget("wnd_main")
        config.window_default_size = win.get_size()
        gtk.main_quit()

class ImageWindow:

    def __init__(self, photo, iconview):

        self.fullscreen = False

        self._wTree = gtk.glade.XML(resource_filename("marvin.ui", "marvin.glade"), "wnd_display_img")

        signals = {
            "on_wnd_display_img_window_state_event": self._on_window_state_event,
            "on_wnd_display_img_delete_event" : self._on_delete_event,
            "on_wnd_display_img_key_press_event" : self._on_key_press,
            }
        self._wTree.signal_autoconnect (signals)

        self.img_widget = self._wTree.get_widget("img")

        self.iconview = iconview
        self.photo = photo
        self.path = gnomevfs.get_local_path_from_uri (self.photo.uri)

        win = self._wTree.get_widget("wnd_display_img")
        win.set_size_request (800, 600)
        win.show_all()

        self.update_image()

    def _on_delete_event(self, widget, event):
        """

        Arguments:
        - `self`:
        - `widget`:
        - `event`:
        """
        pass

    def _on_window_state_event (self, widget, event):
        "This method should be used for the fullscreen functinality"
        pass


    def _on_key_press(self, widget, event):
        """Handle the key press event of the image window

        Arguments:
        - `self`:
        - `widget`:
        - `event`:
        """
        model = self.iconview.get_model()

        iter = model.get_iter(self.iconview.get_selected_items()[0])

        str_key = gtk.gdk.keyval_name(event.keyval)

        if str_key == 'Right' or str_key == 'Down':
            iter_next = model.iter_next(iter)

            if iter_next == None:
                iter_next = model.get_iter_first()

            self.update_iconview(iter_next)

        elif str_key == 'Left' or str_key == 'Up':
            int_path = int(model.get_string_from_iter(iter)) - 1
            if int_path < 0:
                int_path = 0
            str_path = str("%d" % (int_path))

            iter_next = model.get_iter_from_string(str_path)

            self.update_iconview(iter_next)

        elif str_key == 'plus':
            self.zoom(1.1)

        elif str_key == 'minus':
            self.zoom(0.9)

        elif str_key == '1':
            self.update_image(False)

        elif str_key == 'F11':
            win = self._wTree.get_widget("wnd_display_img")
            if self.fullscreen:
                win.unfullscreen()
                self.fullscreen =False
            else:
                win.fullscreen()
                self.fullscreen = True

        elif str_key.lower() == 's':
            self.update_image()

        elif str_key == 'Escape':
            win = self._wTree.get_widget("wnd_display_img")
            win.destroy()
            return
        else:
            print str_key
            return


    def zoom(self, factor):

        curr_width = self.pixbuf.get_width()
        curr_height = self.pixbuf.get_height()
        scaled_pixbuf = self.pixbuf.scale_simple(int(curr_width*factor), int(curr_height*factor), gtk.gdk.INTERP_BILINEAR)

        self.pixbuf = scaled_pixbuf
        self.img_widget.set_from_pixbuf(self.pixbuf)


    def update_iconview(self, iter_next):

        self.photo = model.get(iter_next, 0)[0]
        self.path = gnomevfs.get_local_path_from_uri (self.photo.uri)

        self.update_image()
        str_path = model.get_string_from_iter(iter_next)
        self.iconview.select_path(str_path)

    def update_image(self, to_wnd_size=True):

        if (to_wnd_size):
            win = self._wTree.get_widget("wnd_display_img")
            size = win.get_size()
            self.pixbuf = gtk.gdk.pixbuf_new_from_file_at_size(self.path, size[0], size[1])
        else:
            self.pixbuf = gtk.gdk.pixbuf_new_from_file(self.path)

        self.img_widget.set_from_pixbuf(self.pixbuf)

class PhotoListThread (threading.Thread):

    def __init__ (self, model, query=None, use_progressbar=False):
        super(PhotoListThread, self).__init__()

        if query is not None:
            self._query = query
        else:
            self._query = "select id, uri from photos order by time desc limit 10"

        self._model = model

    def run (self):

        inicio = datetime.datetime.now()

        global wTree

        gtk.gdk.threads_enter()
        progressbar = wTree.get_widget("progressbar1")
        progressbar.show()
        self._model.clear()
        gtk.gdk.threads_leave()

        conn = connect_to_db()
        c = conn.cursor()
        c.execute (self._query)
        i=0
        for row in c:

            gtk.gdk.threads_enter()
            i += 1
            try:
                p = Photo(row[0], row[1])
                self._model.append([p,
                                    p.pixbuf,
                                    p.original_date.date().strftime("%x")])
            except:
                pass

            progressbar.pulse()
            gtk.gdk.threads_leave()

        gtk.gdk.threads_enter()
        Progressbar.push_message(_("%d pictures found.") % (i))
        conn.close()
        progressbar.hide()
        gtk.gdk.threads_leave()

        fin = datetime.datetime.now()
        print "Took %s load the pictures" % (fin-inicio)

class Progressbar(object):

    @classmethod
    def push_message(cls, msg, context="marvin"):
        statusbar = wTree.get_widget("statusbar1")
        context_id = statusbar.get_context_id(context)
        msg_id = statusbar.push(context_id, msg)
        gobject.timeout_add(10000,Progressbar.remove_from_statusbar,context_id, msg_id)

    @classmethod
    def remove_from_statusbar(cls, context_id, msg_id):
        statusbar = wTree.get_widget("statusbar1")
        statusbar.remove(context_id, msg_id)

        return False #stop being called by gobject.timeout_add


class ImportWindow(object):
    """
    """

    def __init__(self):
        """
        """
        self._thread = None
        glade_file = resource_filename("marvin.ui", "marvin.glade")
        self._wTree = gtk.glade.XML(glade_file, "wnd_photo_import")

        signals = {
            "on_checkbtn_include_subdirs_toggled" : self._on_include_subdir_toggled,
            "on_chooserbtn_dir_to_import_current_folder_changed" : self._on_folder_changed,
            "on_btn_accept_clicked" : self._on_accept_clicked,
            }

        self._wTree.signal_autoconnect(signals)

        list_photos = gtk.ListStore(Photo, gtk.gdk.Pixbuf, str)
        list_photos.clear()
        iconview = self._wTree.get_widget("iconview_preview")
        iconview.set_model(list_photos)
        iconview.set_pixbuf_column(1)
        iconview.set_text_column(2)

        win = self._wTree.get_widget("wnd_photo_import")
        win.show_all()

    def _on_folder_changed(self, widget):
        self.refresh_iconview()

    def _on_include_subdir_toggled(self, togglebutton):
        self.refresh_iconview()

    def refresh_iconview(self):

        uris_list = list()

        uri = self._wTree.get_widget("chooserbtn_dir_to_import").get_current_folder_uri()
        selected_path= gnomevfs.get_local_path_from_uri(uri)
        model = self._wTree.get_widget("iconview_preview").get_model()

        recursive = self._wTree.get_widget("checkbtn_include_subdirs").get_active()

        if recursive:
            for root, dirs, files in os.walk(selected_path):
                for name in files:
                    uri = "file://" + str(root) + "/" + str(name)
                    if gnomevfs.get_mime_type(uri) == "image/jpeg":
                        uris_list.append(uri)
        else:
            for name in os.listdir(selected_path):
                uri = "file://" + str(selected_path) + "/" + str(name)
                if gnomevfs.get_mime_type(uri) == "image/jpeg":
                    uris_list.append(uri)

        if (self._thread is not None) and (not self._thread.is_stopped()):
            self._thread.stop()

        self._thread = PhotoPreviewThread(model, uris_list)
        self._thread.start()

    def _on_accept_clicked(self, button):

        self._wTree.get_widget("wnd_photo_import").set_sensitive(False)

        iconview = self._wTree.get_widget("iconview_preview")
        model = iconview.get_model()

        list_of_photos = list()

        if len(iconview.get_selected_items()) == 0:
            iter = model.get_iter_first()

            while (iter is not None):
                list_of_photos.append(model.get(iter, 0)[0])
                iter = model.iter_next(iter)

        else:
            selected_items = iconview.get_selected_items()
            for path in selected_items:
                iter = model.get_iter(path)
                list_of_photos.append(model.get(iter, 0)[0])

        copy_to_photodir = self._wTree.get_widget("checkbtn_copy").get_active()
        progressbar = self._wTree.get_widget("progressbar_import")
        thread = ImportPhotoThread(list_of_photos, copy_to_photodir, progressbar)
        thread.start()

class AboutDialog(object):
    "Control the about dialog"

    def __init__(self):

        glade_file = resource_filename("marvin.ui", "marvin.glade")
        self._wTree = gtk.glade.XML(glade_file)

        signals = {
            "on_aboutdialog_response" : self._on_response,
            }

        self._wTree.signal_autoconnect(signals)

        win = self._wTree.get_widget("aboutdialog")
        win.show_all()

    def _on_response (self, dialog, response_id):
        "Destroy the dialog when the user click in the close button"
        self._wTree.get_widget("aboutdialog").destroy()

class ImportPhotoThread(threading.Thread):
    "Thread that import the photos to the collection"

    def __init__(self, list_of_photos, copy_to_photodir, progressbar):
        super(ImportPhotoThread, self).__init__()

        self._photos = list_of_photos
        self._copy_to_photodir = copy_to_photodir
        self._progressbar = progressbar

    def run(self):
        self._conn = connect_to_db()
        self._c = self._conn.cursor()

        self._progressbar.show()
        i = 0
        total = float(len(self._photos))
        for photo in self._photos:
            i += 1
            self._import (photo, self._copy_to_photodir)
            self._progressbar.set_fraction(i/total)
            self._progressbar.set_text(_("Imported %d of %d") % (i, int(total)))

        self._conn.commit()
        self._conn.close()

        win = self._progressbar.get_toplevel()
        win.destroy()

    def _import (self, photo, copy_to_photodir):

        if copy_to_photodir:
            new_uri = self._copy_photo(photo.uri)
        else:
            new_uri = photo.uri

        self._c.execute("""insert into photos(uri, time, description, roll_id, default_version_id, rating)
        values (?, ?, '', 0, 1, 0)""", (new_uri, time.mktime(photo.original_date.timetuple()),))

    def _copy_photo(self, uri):

        base = "/apps/f-spot/import"
        client = gconf.client_get_default()
        client.add_dir(base, gconf.CLIENT_PRELOAD_NONE)
        root_photo_dir = client.get_string(base + "/storage_path")

        src_path = gnomevfs.get_local_path_from_uri(uri)
        f = open(src_path, 'rb')
        tags = EXIF.process_file(f, stop_tag='DateTimeOriginal')

        if tags.has_key('EXIF DateTimeOriginal'):
            (aux_date, aux_time) = str(tags['EXIF DateTimeOriginal']).split(' ')
            (year, month, day) = aux_date.split(':')

        else:
            mtime = time.localtime(os.path.getmtime(src_path))
            year = str(mtime[0])
            moth = str(mtime[1])
            day = str(mtime[2])


        destdir = os.path.join(root_photo_dir, year, month, day)
        list_dir = (os.path.join(root_photo_dir, year),
                    os.path.join(root_photo_dir, year, month),
                    os.path.join(root_photo_dir, year, month, day))
        for aux_path in list_dir:
            if not os.path.exists(aux_path):
                os.mkdir(aux_path)

        if os.path.exists(os.path.join(destdir, os.path.basename(src_path))):
            aux = os.path.basename(src_path).rsplit('.', 1)
            suff = str(time.localtime()[3]) + str(time.localtime()[4])
            destdir = os.path.join(destdir, aux[0] + suff + aux[1])
        else:
            destdir = os.path.join(destdir, os.path.basename(src_path))

        shutil.copyfile(src_path, destdir)

        return "file://" + destdir #returns the destination uri

class PhotoPreviewThread(threading.Thread):
    """Thread that loads the list store associated to the iconview that shows
    the preview of the photos that will be loaded

    """

    def __init__(self, model, uris):
        super(PhotoPreviewThread, self).__init__()

        self._model = model
        self._uris = uris
        self._stop = False
        self._stopped = True

    def run(self):
        self._stopped = False
        self._model.clear()

        for uri in self._uris:
            if self._stop:
                self._stopped = True
                self._stop = False
                return

            photo = Photo(-1, uri, 150)
            self._model.append([photo,
                                photo.pixbuf,
                                photo.original_date.date().strftime("%x")])

    def stop(self):
        self._stop = True

    def is_stopped(self):
        return self._stopped

def setup_log():
    "Configure the log system"
    # code adapted from
    # http://docs.python.org/lib/multiple-destinations.html example
    # set up logging to file - see previous section for more details
    logging.basicConfig(level=logging.DEBUG,
                        #format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        format='%(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)


def start_import(run_main_loop=True):
    "Initialize the import dialog"

    ImportWindow()

    if run_main_loop:
        setup_log()

        gobject.set_application_name(_("Photo collection manager"))
        gobject.set_prgname("marvin")

        gtk.main()

def start():
    "Entry point to start the application"

    setup_log()
    log = logging.getLogger('view')

    gobject.set_application_name(_("Photo collection manager"))
    gobject.set_prgname("marvin")

    wnd = MainWindow()

    gtk.gdk.threads_enter()
    gtk.main()
    gtk.gdk.threads_leave()


if __name__ == '__main__':
    start()
