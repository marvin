##
## model.py
## Login : <freyes@yoda>
## Started on  Tue Jul  1 20:50:04 2008 Felipe Reyes
## $Id$
##
## Copyright (C) 2008 Felipe Reyes
##
## This file is part of Marvin.
##
## Marvin is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Marvin is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gobject
import gtk
import gtk.gdk
import gnomevfs
import gconf
import md5
import sqlite3
import os.path
import datetime
import time

##logging system
import logging
log = logging.getLogger('model')

try:
    import EXIF
except:
    curr_dir = os.path.abspath(".")
    sys.path.append(curr_dir)
    import EXIF


class Photo(gobject.GObject, object):
    """Contains the information of a photo
    """
    def __init__(self, id, uri, pixbuf_size=64, cursor=None):
        """

        Arguments:
        - `id`: the id of the photo
        """
        gobject.GObject.__init__(self)

        if not os.path.isfile(gnomevfs.get_local_path_from_uri(uri)):
            print gnomevfs.get_local_path_from_uri(uri)
            raise RuntimeError("File %s not found" % (uri))

        self._id = id
        self.uri = uri

        f = open(self.path, 'rb')
        tags = EXIF.process_file(f, stop_tag='DateTimeOriginal')

        if tags.has_key('EXIF DateTimeOriginal'):
            (aux_date, aux_time) = str(tags['EXIF DateTimeOriginal']).split(' ')
            (year, month, day) = aux_date.split(':')
            (hour, minute, secs) = aux_time.split(':')
            self.original_date = datetime.datetime(int(year),
                                                   int(month),
                                                   int(day),
                                                   int(hour),
                                                   int(minute),
                                                   int(secs))
        else:
            self.original_date = time.localtime(os.path.getmtime(self.path))

        self.tags = list()

        if (cursor is not None) and (id > -1):
            c.execute("select tag_id from photo_tags where id = ?", self._id)
            for row in c:
                c.execute("select name from tags where id=?", row[0])
                for tag in c:
                    self.tags.append(tag[0])

        self.thumbnail_path = thumbnail_path_from_uri(self.uri, "large")

        try:
            self.pixbuf = gtk.gdk.pixbuf_new_from_file_at_size (self.thumbnail_path, pixbuf_size, pixbuf_size)
        except:
            thumb_pixbuf = gtk.gdk.pixbuf_new_from_file_at_size(self.path, 256, 256)
            thumb_pixbuf.save(self.thumbnail_path, "png", {"quality":"100"})
            self.pixbuf = gtk.gdk.pixbuf_new_from_file_at_size (self.thumbnail_path, pixbuf_size, pixbuf_size)
            log.info("Thumbnail for %s created" % self.uri)

    def create_thumbnail(self):
        cmd = "convert '%s' -resize 256x256 '%s'" % (self.path, thumbnail_path_from_uri(self.uri, "large"))
        print cmd
        os.system(cmd)

    ### properties

    # path
    def get_path(self):
        return gnomevfs.get_local_path_from_uri(self.uri)

    path = property(get_path, None, None, "the path where the photo is stored")

gobject.type_register(Photo)

class PhotoListStore (gtk.ListStore):

    def __init__ (self):

        super(PhotoListStore, self).__init__(Photo)

gobject.type_register(PhotoListStore)

def thumbnail_path_from_uri(uri, size='normal'):
    """Construct the path for the thumbnail of the given uri

    Arguments:
    - `uri`: the uri that points to the file that is looking for the thumbnail.
    - `size`: the size of the thumbnail (normal or large)
    """

    assert isinstance(uri, basestring), \
           TypeError("The uri must be a str")

    assert isinstance(size, basestring) and (size == 'large' or size=='normal'), \
           TypeError("The size for thumbnail can be normal or large")

    hash = md5.new()
    hash.update(uri)
    path = os.path.join(os.path.expanduser('~'), ".thumbnails", size, str("%s.png" % hash.hexdigest()))

    return path

def connect_to_db():
    db_path = os.path.join(os.path.expanduser('~'), ".gnome2", "f-spot", "photos.db")
    conn = sqlite3.connect(db_path)
    return conn

class ConfigurationManager(gobject.GObject):
    """
    """

    def __init__(self):
        """
        """
	super(ConfigurationManager, self).__init__()
        self._client = gconf.client_get_default()
        self._base = "/apps/marvin"
        self._client.add_dir (self._base, gconf.CLIENT_PRELOAD_NONE)


    #### properties
    # window_default_size
    def set_window_default_size(self, value):

        assert isinstance(value, tuple) and len(value)==2, "The value must be a tuple of int, like (200, 200)"
        assert isinstance(value[0], int) and value[0] > 0, "The pair must int > 0"
        assert isinstance(value[1], int) and value[1] > 0, "The pair must int > 0"

        self._client.set_pair(self._base + "/window_size",
                              gconf.VALUE_INT, gconf.VALUE_INT,
                              value[0], value[1])

    def get_window_default_size(self):
        return self._client.get_pair(self._base + "/window_size",
                                     gconf.VALUE_INT, gconf.VALUE_INT)

    window_default_size = property(get_window_default_size,
                                   set_window_default_size,
                                   None,
                                   "The default size that will use the main window")

    @classmethod
    def set_wallpaper(cls, path):

        base = "/desktop/gnome/background"
        client = gconf.client_get_default()
        client.add_dir(base, gconf.CLIENT_PRELOAD_NONE)

        client.set_string(base + "/picture_filename", path)

gobject.type_register(ConfigurationManager)
