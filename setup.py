#!/usr/bin/python2.5
##
## setup.py
## Login : <freyes@yoda.>
## Started on  Fri Dec 14 22:14:38 2007 Felipe Reyes
## $Id$
##
## Copyright (C) 2007 Felipe Reyes <felipereyes@gmail.com>
##
## This file is part of Rascase.
##
## Rascase is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Rascase is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
##

from setuptools import setup, find_packages

setup(name="Marvin",
      version='0.1',
      description="A simple photo manager",
      author="Felipe Reyes",
      author_email="felipereyes@gmail.com",
      license='GPL v3 or later',
      packages=find_packages(),
      package_data={
        'marvin':
            ['ui/*',]
        },
      entry_points = {'console_scripts':
                      ['marvin = marvin.view:start',]
                      }
      )
